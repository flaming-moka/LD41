extends Node2D

var is_ending = true

func _on_Growl_finished():
	if is_ending:
		yield(get_tree().create_timer(5.0), "timeout")
		self.scale = Vector2(1, 1)
		$TextureRect.texture = load("res://Assets/Art/credits.png")
