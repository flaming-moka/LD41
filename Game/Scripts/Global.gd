extends Node

signal exposed

# monster params
var pet_name = "Ludy"
var hunger = 50
var mood = 50
var stench = 10
var poop_count = 0
var growth = 0
var poo_slots = [0, 0, 0, 0, 0]

var money = 30
var day =  1 # test
var timer
var actions_count = 0
var purchase_count = 0

var food_prices = {
	"Chicken" : 4,
	"Steak" : 6,
	"Milk" : 2,
	"Tuna" : 4
}
# food stats except price and maluses
var food_stats = {
	"Chicken" : {"hunger" : -30, "bonus" : 20},
	"Steak" : {"hunger" : -60, "bonus" : 40},
	"Milk" : {"hunger" : -20, "bonus" : 5},
	"Tuna" : {"hunger" : -50, "bonus" : 10}
}
# dialogues
var home_dialogues = {
	1 : ['"Mom, dad, I’m home!"', '"Welcome back darling!"', '"I’m going to play!"'],
	2 : ['"I’m home!"', '"Welcome back!"', "I’m going to play!"],
	3 : ['"I’m home!"', '"Welcome back darling!"', '"I’m going to play!"']
}

func _ready():
	OS.window_maximized = true

func increase_day():
	day += 1
	actions_count = 0
	purchase_count = 0
	if (day % 7) == 0:
		money += 50
	# other stats
	mood = clamp(mood - 20, 0, 100)
	if growth <= 2: # baby monster
		poop_count += 1
		hunger = clamp(hunger + 40, 0, 100)
	else:
		poop_count += 2
		hunger = clamp(hunger + 60, 0, 100)
	# check stench
	stench = clamp(stench + (poop_count * 30), 0, 100)
	# change scene based on day
	if day < 10:
		get_tree().change_scene("res://Scenes/House.tscn")
	else:
		get_tree().change_scene("res://Scenes/HouseFinal.tscn")




