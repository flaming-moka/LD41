extends Sprite

func _ready():
	randomize()
	call_anim()

func call_anim():
	$AnimationPlayer.play("flickering")
	$Neon.play()
 
func _on_AnimationPlayer_animation_finished(anim_name):
	var t = (randi() % 5) + 2
	$Timer.wait_time = t
	$Timer.start()

func _on_Timer_timeout():
	call_anim()
