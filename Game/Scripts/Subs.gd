extends Label

var text_queue = PoolStringArray([])
var is_busy = false
var text_time setget set_time

func _ready():
	pass

func set_font_size(new_size):
	self.get_font("font").size = new_size

func set_time(new_value):
	text_time = new_value
	$Timer.wait_time = text_time

func text_requested(content):
	if typeof(content) == TYPE_STRING:
		text_queue.append(content)
	elif typeof(content) == TYPE_ARRAY:
		for i in content.size():
			text_queue.append(content[i])

	if is_busy:
		return
	else:
		is_busy = true
		self.text = text_queue[0]
		text_queue.remove(0)
		$Timer.start()

func _on_Timer_timeout():
	is_busy = false
	if text_queue.size() > 0:
		text_requested(null)
	else:
		self.text = ""
