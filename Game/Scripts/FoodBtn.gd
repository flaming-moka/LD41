extends TextureButton

export (PackedScene) var ActionPopup

var price
var t = []
onready var Subs = get_parent().get_parent().get_node("Subs")

func _ready():
	price = Global.food_prices[name]

func _on_Button_pressed():
	if Global.money >= price:
		give_food()
	else:
		Subs.text_requested('"Damn! I’m out of money..."')

func give_food():
	# sound
	$Sound.play()
	Global.purchase_count += 1
	# subs
	if Global.purchase_count == 2:
		Subs.text_time = 2.5
		Subs.text_requested('"I shouldn’t buy too much food, or I’ll run out of pocket money..."')
	# popup
	var a = ActionPopup.instance()
	a.rect_position += Vector2(-100, 0)
	# bonus
	Global.hunger = clamp(Global.hunger + Global.food_stats[name]["hunger"], 0, 100)
	Global.mood = clamp(Global.mood + Global.food_stats[name]["bonus"], 0, 100)
	a.get_node("Label").text = "+%s%% Mood" % String(Global.food_stats[name]["bonus"]) + "\n%s%% Hunger" % String(Global.food_stats[name]["hunger"])
	# malus
	Global.money -= price
	if name == "Tuna":
		Global.stench = clamp(Global.stench + 20, 0, 100)
		if Global.stench >= 70:
			Subs.text_requested('"Ugh! You smell so bad!"')
			Subs.text_requested('"I should clean you or my parents will find you."')
	elif name == "Steak":
		# hunger regen
		pass
	elif name == "Milk":
		Global.poop_count += 2
	add_child(a)
