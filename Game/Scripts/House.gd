extends Sprite

signal text_requested

var t1 = []
var can_zoom = false
var Fade = preload("res://Scenes/FadeBlack.tscn")
var path_speed = 2

func _ready():
	randomize()
	# texture and music
	if Global.day >= 8 and Global.day < 10:
		self.texture = load("res://Assets/Art/backgrounds/house-dark.png")
	if Global.day < 8:
		$Music.play()
	else:
		$Music.queue_free()
	# fade in
	var f = Fade.instance()
	f.can_show = true
	f.fade_time = 1.5
	f.position = Vector2(-960, -540)
	$Path/PathFollow2D/Child/Camera2D.add_child(f)
	# music fix
	$Path/FirstStep.interpolate_property($Path/PathFollow2D, "unit_offset", 0, 0.3, path_speed/2, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Path/FirstStep.start()
	$Path/PathFollow2D/Child.vel = Vector2(0, -1)
	# set random dialogue
	if Global.day < 7:
		var key = (randi() % 3) + 1
		for value in Global.home_dialogues[key].size():
			t1.append(Global.home_dialogues[key][value])
	# show subs
	$Path/PathFollow2D/Child/Camera2D/Subs.set_font_size(56)
	$Path/PathFollow2D/Child/Camera2D/Subs.text_time = 2
	if Global.day == 7:
		$Path/PathFollow2D/Child/Camera2D/Subs.text_requested(['"I can’t wait for Tim to come!"', '"Today he looked so excited when I told him about %s!"' % Global.pet_name])
	elif Global.day == 8:
		$Path/PathFollow2D/Child/Camera2D/Subs.text_requested(['"Mom, dad, I’m home!"', '"...Mom? Dad?"', '"...they’re probably outside."'])
	elif Global.day == 9:
		$Path/PathFollow2D/Child/Camera2D/Subs.text_requested(['"Mom, dad, I’m home!', '"..."', '"...They must have gone to see grandma."', '"I’m sure of it."'])
	elif Global.day == 10:
		$Path/PathFollow2D/Child/Camera2D/Subs.text_requested('"..."')

func init(req):
	t1.append(req)

func _on_FirstStep_tween_completed(object, key):
	# trigger dialogue
	if t1.size() > 0:
		self.connect("text_requested", $Path/PathFollow2D/Child/Camera2D/Subs, "text_requested")
		emit_signal("text_requested", t1)
		yield(get_tree().create_timer(2.0 * (t1.size() - 1)), "timeout")
	# start 2nd tween
	$Path/PathFollow2D/Child.vel = Vector2(1, 0)
	$Path/SecondStep.interpolate_property($Path/PathFollow2D, "unit_offset", 0.3, 1, path_speed, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Path/SecondStep.start()

func _on_SecondStep_tween_completed(object, key):
	can_zoom = true # final scene stuff
	$Walls.set_collision_layer_bit(1, true)
	$Walls.set_collision_mask_bit(0, true)
	$Path/PathFollow2D/Child.vel.x = 0
	$Path/PathFollow2D/Child.can_move = true
	# close the door
	$Door.queue_free()
	$DoorSound.play()

func _on_Basement_body_entered(body):
	$DoorSound.play()
	get_tree().change_scene("res://Scenes/Main.tscn")


