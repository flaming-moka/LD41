extends KinematicBody2D

export (int) var speed
var dir
var vel = Vector2()
var can_move = false
var anim = "run-right"

func _ready():
	pass

func _physics_process(delta):
	if can_move:
		dir = Vector2()
		if Input.is_action_pressed("ui_right"):
			dir += Vector2(1, 0)
			$Sprite.flip_h = false
		elif Input.is_action_pressed("ui_left"):
			dir -= Vector2(1, 0)
			$Sprite.flip_h = true
		if Input.is_action_pressed("ui_down"):
			dir += Vector2(0, 1)
		elif Input.is_action_pressed("ui_up"):
			dir -= Vector2(0, 1)
		vel = dir.normalized() * speed * delta
		move_and_collide(vel)
	# animation
	if vel.x == 0:
		anim = "idle"
	else:
		anim = "run_right"
	if vel.y < 0:
		anim = "run_top"
	$Sprite.play(anim)