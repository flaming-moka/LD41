extends ProgressBar

onready var color = Color(modulate.r, modulate.g, modulate.b)

func _ready():
	if name == "ProgressBar2":
		if value > 70:
			play_animation($FirstAnim)
	else:
		if value < 30:
			play_animation($FirstAnim)

func _on_ProgressBar_value_changed(value):
	if name == "ProgressBar2":
		if value > 70:
			play_animation($FirstAnim)
	else:
		if value < 30:
			play_animation($FirstAnim)

func _on_FirstAnim_tween_completed(object, key):
	play_animation($SecondAnim)

func _on_SecondAnim_tween_completed(object, key):
	if name == "ProgressBar2":
		if value > 70:
			play_animation($FirstAnim)
	else:
		if value < 30:
			play_animation($FirstAnim)

func play_animation(tween):
	if tween.name == "FirstAnim":
		color.a = 0.0 # final value
		tween.interpolate_property(self, "modulate", modulate, color, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	else:
		color.a = 1.0
		tween.interpolate_property(self, "modulate", modulate, color, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()