extends TextureButton

export (PackedScene) var ActionPopup

const MAX_ACTIONS = 3
onready var parent = get_parent()
onready var Subs = get_parent().get_parent().get_node("Subs")
onready var Fade = preload("res://Scenes/FadeBlack.tscn")
var poo_cleaned = 3

func _ready():
	if Global.day > 7 and name == "Love":
		self.disabled = true

func _on_ActionBtn_pressed():
	increase_actions()
	
	var a = ActionPopup.instance()
	a.rect_position += Vector2(50, 0)
	if name == "Exit":
		if Global.day < 7:
			Subs.text_requested('"See you later, %s!"' % Global.pet_name)
			# fade to black and next day
			var f = Fade.instance()
			f.fade_out = false
			get_tree().root.get_node("Main").add_child(f)
			yield(get_tree().create_timer(Subs.get_node("Timer").wait_time), "timeout")
		
		Global.increase_day()
	elif name == "Love":
		Global.mood = clamp(Global.mood + 15, 0, 100)
		a.get_node("Label").text = "+ 80% Mood"
		Subs.text_time = 1
		Subs.text_requested('"Good boy!"')
	elif name == "Wash":
		if Global.growth == 0:
			Global.growth = 1
			get_tree().root.get_node("Main/Position2D/Monster").clean_egg()
		Global.stench = clamp(Global.stench - 80, 0, 100)
		Global.mood = clamp(Global.mood - 15, 0, 100)
		a.get_node("Label").text = "-80% Stench\n-15% Mood"
	elif name == "Poop":
		var starting_stench = Global.stench
		Global.poop_count = clamp(Global.poop_count - poo_cleaned, 0, 5)
		Global.stench = clamp(Global.stench - (30 * poo_cleaned), 0, 100)
		var final_stench = Global.stench
		a.get_node("Label").text = "-%s%% Stench" % String(starting_stench - final_stench)
		if Global.poop_count == 0:
			self.disabled = true
		# clean poop
		var poo = get_tree().root.get_node("Main/Poo")
		var k = 0
		for i in poo.get_child_count():
			if poo.get_child(i).get_child_count() > 0:
				Global.poo_slots[k] = 0
				k += 1
				poo.get_child(i).get_child(0).queue_free()
	add_child(a)

func increase_actions():
	Global.actions_count += 1
	if Global.actions_count == MAX_ACTIONS:
		if name != "Exit":
			for b in range(1, parent.get_child_count() - 1):
				parent.get_children()[b].disabled = true
		else:
			return