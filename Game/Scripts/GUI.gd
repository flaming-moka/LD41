extends Control

func _ready():
	pass

func _process(delta):
	$MarginContainer/Top/Label2.text = "Day %s" %String(Global.day)
	$MarginContainer/Top/Label.text = "%s $" %String(Global.money)
	$MarginContainer/Top/Mood/ProgressBar.value = Global.mood
	$MarginContainer/Top/Mood/ProgressBar2.value = Global.stench
	$MarginContainer/Top/Info/ProgressBar3.value = 100 - Global.hunger