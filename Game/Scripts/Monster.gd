extends Node2D

var textures = ["res://Assets/Art/Monster/monster1-egg.png", "res://Assets/Art/Monster/monster1.png",
			"res://Assets/Art/Monster/monster2.png", "res://Assets/Art/Monster/monster3.png",
			"res://Assets/Art/Monster/monster4.png", "res://Assets/Art/Monster/monster5.png"]
onready var Subs = get_tree().root.get_node("Main/GUI/MarginContainer/Subs")
onready var music = get_tree().root.get_node("Main/Music")
var music_pos = 0.0

func _ready():
	# growth
	if Global.day == 1:
		Global.growth = 0
	elif Global.day == 2:
		Global.growth = 1
	elif Global.day == 3 or Global.day == 4:
		Global.growth = 2
	elif Global.day == 5 or Global.day == 6:
		Global.growth = 3
	elif Global.day == 7 or Global.day == 8:
		Global.growth = 4
	elif Global.day == 9:
		Global.growth = 5
	$Sprite.animation = String(Global.growth)
	$Sprite.play()
	# sounds
	if Global.day == 4:
		yield(get_tree().create_timer(4.0), "timeout")
		play_sound($Mommy, 1.0)
	if Global.day == 6:
		yield(get_tree().create_timer(4.0), "timeout")
		play_sound($FeedMe, 1.5)
	if Global.day == 8:
		yield(get_tree().create_timer(4.0), "timeout")
		$FeedMe.play()

func play_sound(sound, pause):
	# stop main music and play the sound
	music_pos = music.get_playback_position()
	music.stop()
	yield(get_tree().create_timer(pause), "timeout")
	sound.play()

func _on_Mommy_finished():
	yield(get_tree().create_timer(1.0), "timeout")
	music.play(music_pos)
	Subs.text_requested('"Whoa..."')

func _on_FeedMe_finished():
	if Global.day == 8:
		pass
	else:
		music.play(music_pos)
		Subs.text_requested('"Did you just say... ‘feed me’?"')

func clean_egg():
	$Sprite.animation = String(Global.growth)
	$Sprite.play()


