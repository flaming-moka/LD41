extends Sprite

export (int) var fade_time
var black = Color(0, 0, 0, 1)
var transparent = Color(0, 0, 0, 0)
var fade_out = true
var can_show = false

func _ready():
	if can_show:
		$Node2D/Label.text = "Day %s" % String(Global.day)
	$Timer.wait_time = fade_time
	if fade_out:
		fade_in()
	else: 
		fade_out()

func fade_out():
	$Tween.interpolate_property($TextureRect, "modulate", transparent, black, fade_time, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()

func fade_in():
	$Tween2.interpolate_property($TextureRect, "modulate", black, transparent, fade_time, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween2.start()

func _on_Tween_tween_completed(object, key):
	can_show = false
	queue_free()

func _on_Tween2_tween_completed(object, key):
	can_show = false
	queue_free()
