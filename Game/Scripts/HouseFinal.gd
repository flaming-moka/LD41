extends "res://Scripts/House.gd"

onready var cam = $Path/PathFollow2D/Child/Camera2D
var zoom_speed = 0.07
const MAX_ZOOM = 1.1
const MIN_ZOOM = 1.4
const MIN_DB = -10
const MAX_DB = 24
var h_speed = 2 # dB per sec

func _ready():
	self.texture = load("res://Assets/Art/backgrounds/house-end.png")
	$Music.stop()
	$Path/PathFollow2D/Child.speed = 400

func _process(delta):
	if can_zoom:
		if Input.is_action_pressed("ui_right"):
			cam.zoom.x -= zoom_speed * delta
			cam.zoom.y -= zoom_speed * delta
			$HeartBeat.volume_db += h_speed * delta
		elif Input.is_action_pressed("ui_left"):
			cam.zoom.x += zoom_speed * delta
			cam.zoom.y += zoom_speed * delta
			$HeartBeat.volume_db -= h_speed * delta

	cam.zoom.x = clamp(cam.zoom.x, MAX_ZOOM, MIN_ZOOM)
	cam.zoom.y = clamp(cam.zoom.y, MAX_ZOOM, MIN_ZOOM)
	$HeartBeat.volume_db = clamp($HeartBeat.volume_db, MIN_DB, MAX_DB)

func _on_Area2D_body_entered(body):
	$Path/PathFollow2D/Child.set_physics_process(false)
	$Path/PathFollow2D/Child/Sprite.play("idle_top")
	yield(get_tree().create_timer(1.0), "timeout")
	get_tree().change_scene("res://Scenes/Black.tscn")
