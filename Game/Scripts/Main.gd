extends Node

export (PackedScene) var Fade
export (PackedScene) var Monster
export (PackedScene) var Black
export (PackedScene) var LightFlicker

onready var Subs = $GUI/MarginContainer/Subs
var textures = ["res://Assets/Art/backgrounds/cellar.png", "res://Assets/Art/backgrounds/cellar2.png", "res://Assets/Art/backgrounds/cellar3.png"]
var dialogues = {
	1 : '"Hah! It hatched!!"',
	3 : '"Hey, %s! You grow so fast!"' % Global.pet_name,
	5 : '"Wow, I fear this basement will soon be too small for you..."'
}
var b # black
var f #fade

func _ready():
	# set the correct font size
	Subs.set_font_size(32)
	# music volume adjustment
	if $Music.stream.resource_path == "res://Assets/Music/cellar2.ogg":
		$Music.volume_db = 30
	elif $Music.stream.resource_path == "res://Assets/Music/cellar3.ogg":
		$Music.volume_db = 35
	# music
	if Global.day == 7:
		$Music.stop()
	elif Global.day == 8:
		$Music.stream = load("res://Assets/Music/cellar2.ogg")
		$Music.play()
	elif Global.day >= 9:
		$Music.stream = load("res://Assets/Music/cellar3.ogg")
		$Music.play()
	# monster
	var m = Monster.instance()
	$Position2D.add_child(m)
	# fading
	var f = Fade.instance()
	f.fade_time = 2
	add_child(f)
	# cellar texture
	if (Global.day >= 3 and Global.day <= 5) and (Global.mood < 20 or Global.hunger > 80):
		$TextureRect.texture = load(textures[1])
	if Global.day >= 5 and Global.day <= 7:
		$TextureRect.texture = load(textures[1])
	elif Global.day >= 8:
		$TextureRect.texture = load(textures[2])
	# spawn poo
	var poo = $Poo
	for i in Global.poop_count:
		var s = Sprite.new()
		s.texture = load("res://Assets/Art/poop.png")
		for k in poo.get_child_count():
			if Global.poo_slots[k] == 0:
				Global.poo_slots[k] = 1
				poo.get_child(k).add_child(s)
				break
	# wait for the fade in to finish
	yield(get_tree().create_timer(2.0), "timeout")
	# messages, story-based ones go first
	if Global.day == 1:
		Subs.text_requested(dialogues[Global.day])
		Subs.text_time = 3
		Subs.text_requested(['"So cute! You’ll be my secret pet!"', '"I will call you %s!"' % Global.pet_name, '"I should clean you from the remains of the egg,"', '"let me use this yellow brush."', '"Are you hungry? I have some milk"'])
	if Global.day == 3 or Global.day == 5:
		Subs.text_requested(dialogues[Global.day])
	elif Global.day == 7:
		tim_cutscene()
	elif Global.day >= 8:
		var l = LightFlicker.instance()
		add_child(l)
	else:
		$GUI/MarginContainer/Subs.text_requested('"Hey, %s! How are you?"' % Global.pet_name)
	if Global.stench >= 70:
		Subs.text_requested('"Ugh! You smell so bad! I should clean you or my parents will find you."')
	if Global.hunger > 60 and Global.hunger < 80:
		Subs.text_requested('"Damn, %s looks hungry again"' % Global.pet_name)
	if Global.mood <= 35:
		Subs.text_requested('"It seems to be upset... I should do something. Or %s might make a mess while I’m at school!"' % Global.pet_name)

func tim_cutscene():
	$Timmy.show()
	$EatSound.play()
	# disable buttons
	for i in range(1, $GUI/MarginContainer/Actions.get_child_count()):
		$GUI/MarginContainer/Actions.get_child(i).disabled = true
	for i in range(1, $GUI/MarginContainer/Food.get_child_count()):
		$GUI/MarginContainer/Food.get_child(i).disabled = true
	Subs.text_requested(['"Wow!! This is incredible!"', '"Can I touch it?"', '"Sure!"'])
	Subs.text_time = 1.5
	yield(get_tree().create_timer(6.5), "timeout") # wait 4 the dialogue
	# black screen
	b = Black.instance()
	b.is_ending = false
	add_child(b)

func _on_EatSound_finished():
	$Timmy.hide()
	Global.hunger = 0
	yield(get_tree().create_timer(4.0), "timeout")
	Subs.text_time = 2
	Subs.text_requested('"Why... Why... What have you done...Tim..."')
	# enable btns
	for i in range(1, $GUI/MarginContainer/Actions.get_child_count()):
		$GUI/MarginContainer/Actions.get_child(i).disabled = false
	for i in range(1, $GUI/MarginContainer/Food.get_child_count()):
		$GUI/MarginContainer/Food.get_child(i).disabled = false
	# fading
	f = Fade.instance()
	f.fade_time = 2
	add_child(f)
	b.queue_free()
	# play sound
	yield(get_tree().create_timer(2.0), "timeout")
	$FeedMore.play()

func _on_FeedMore_finished():
	$Music.stream = load("res://Assets/Music/cellar2.ogg")
	$Music.volume_db = 10
	$Music.play()



