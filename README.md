<h3> Welcome! </h3>
This is Flaming Moka's entry for the **41st Ludum Dare jam!**

<h1> Feed me </h1>
The title of our game is **Feed Me**: a life simulator mixed with... oh well, we don't want to spoil the surprise!
<h3>Summary</h3>A child has found a mysterious egg. The creature which has hatched from it grows surrounded by love, but will it be enough to satisfy its hunger?

This was our first time joining the the Ludum Dare. A lot could have been done better; still, we're very satisfied with the result!

<h1> Credits </h1>
**Feed Me** is proudly developed with [Godot Engine](https://godotengine.org/) by the duo **Flaming Moka**: [Léonie Aonzo](http://leonieaonzo.com) (design, art, sound) + Davide Cortellucci (design, developement).
We thank AlgoTunes, iut_Paris8 and klankbeeld for the music tracks they shared on [Freesound](https://freesound.org/).
We're also grateful to turkkub, Freepik and Nhor Phai for sharing their beautiful icons on Flaticon!


<h1>Bugfixes</h1>
Changelog of the updates completed two days after the submission:
- Fixed credits bug
- Fixed hunger level bug on day 7
- Fixed flickering light bug in the last background
- Fixed monster order
- Adjusted music volume
- Improved subtitles and ui legibility (timing, size, percentages)
- Fixed poo spawn after milk
- Fixed subtitle bug on day 4
- Stopped music bug when coming back in the house after day 7
- Adjusted child speed during cut scene
- Restored correct steak icon
- Fixed popup bug for food icons


Changelog for the second update, May 1st 2018:
- Maximized game's screen
- Fixed progress bars